<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '');

/** MySQL database username */
define('DB_USER', '');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+i 2^R/$wa+X$ZxT$b7r|VkYvie-P^nu~sX=o&8w~[8rQ)u3{S@N%P=qehIULq)S');
define('SECURE_AUTH_KEY',  'P-eG&XRH>9&a%;d !OITPb)gkRxwyfmW5iZRvL]|v:k@V+BqE9j5X?2@N9S&3vZ=');
define('LOGGED_IN_KEY',    'J!-SH[;+>Xuz3XiW*1}bU=!*Bbk3Qc6K F3R6Hy<9c9gW$p=fVl4s-p}JC>b+^_@');
define('NONCE_KEY',        '+{15b+G*z&=H.{j}`E2q3|)c0y75pC@n5=Z}.H xIS8]MHz).wvuL,Tvc+BtDl,O');
define('AUTH_SALT',        'gOBs_-CoZ?ncWQZ6d+hN+W $<g77S3BP$|,M4_u,Qa?O~+`ZiHg~r.`4)p{y9>E)');
define('SECURE_AUTH_SALT', 'sq)k9m-503wA`W#0oe1:9A)FlYN<}*oLuRT1+U?FcFGA&#^rFU#f_Q=H/1~FbWg.');
define('LOGGED_IN_SALT',   '6#47ZB7:RP+be$B u>U:Ap$bU1w3p|i9gi$A,H`l0+QGGu[p1_0a:z#{K2|BZyp?');
define('NONCE_SALT',       '>+/OE74cuSVV|~/{8q{dJaCA%e7X1|@ H/AFEi,%j3&%Cq-~ez;4[lJ.=h-.(F;)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');


define("WP_MEMORY_LIMIT", "64M");


/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
